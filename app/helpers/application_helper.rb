# frozen_string_literal: true

module ApplicationHelper
  def character_comics_page_url(page_number)
    character_comics_path(
      params
        .permit('name')
        .merge(
          page: page_number
        )
    )
  end
end
