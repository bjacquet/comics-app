
window.upvoteComic = function(comicId) {
  let heartOn = document.getElementById(`heartOn${comicId}`)
  voteSentiment = heartOn.classList.contains("invisible")

  Rails.ajax({
    url: "/favourites/upvote",
    type: "POST",
    data: `comic_id=${comicId}&vote_sentiment=${voteSentiment}`,
    success: function(data) {
      voteSentiment ?
        heartOn.classList.remove("invisible")
        :
        heartOn.classList.add("invisible")

      let div = document.getElementById(comicId)
      div.classList = window.frame_cover(data.votes)
    }
  })
}

window.frame_cover = function(upvotes) {
  return (upvotes > 0) ? "relative border-8 border-red-700" : "relative"
}
