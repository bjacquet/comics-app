# frozen_string_literal: true

class QueryMarvel
  REQUEST_LIMIT = 20

  def initialize
    @marvel_api = Marvel::Client.new
  end

  def fetch_characters(character_name)
    response = @marvel_api.get_characters_by_name(character_name)
    parse_response(response)
  end

  def fetch_comics_by_character(character_ids, options = {})
    options[:offset] = REQUEST_LIMIT * (options.delete(:page).to_i - 1) if options[:page]

    response = @marvel_api.get_comics_by_characters(character_ids, options)
    parse_response(response)
  end

  private

  def parse_response(response)
    data = response['data']
    result = {
      results: data['results']
    }

    return result if result[:results].empty?

    current_page = current_page(data)
    result.merge(
      {
        current_page: current_page,
        next_page: next_page(current_page),
        previous_page: previous_page(current_page),
        total_pages: total_pages(data)
      }
    )
  end

  def current_page(data)
    (data['offset'] + REQUEST_LIMIT) / REQUEST_LIMIT
  end

  def next_page(current_page)
    current_page + 1
  end

  def previous_page(current_page)
    current_page - 1
  end

  def total_pages(data)
    data['total'] / REQUEST_LIMIT
  end
end
