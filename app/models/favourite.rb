# frozen_string_literal: true

class Favourite < ApplicationRecord
  after_save :prevent_negative_votes

  def self.vote_for_favourite(marvel_comic_id, vote_sentiment)
    vote = vote_sentiment ? 1 : -1
    ActiveRecord::Base.transaction do
      favourite = Favourite.find_or_initialize_by(marvel_comic_id: marvel_comic_id)
      favourite.upvotes += vote
      favourite.save
    end
  end

  private

  def prevent_negative_votes
    self.upvotes = 0 if upvotes.negative?
  end
end
