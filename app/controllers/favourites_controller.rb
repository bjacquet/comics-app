# frozen_string_literal: true

class FavouritesController < ApplicationController
  before_action :permit_params

  def upvote
    Favourite.vote_for_favourite(@params[:comic_id], @params[:vote_sentiment])

    fav = Favourite.find_by(marvel_comic_id: @params[:comic_id])

    render json: { message: 'Upvoted!', votes: fav.upvotes }, status: :ok
  end

  private

  def permit_params
    @params ||= params.permit(:comic_id, :vote_sentiment)
    @params[:vote_sentiment] = @params[:vote_sentiment] == 'true'
  end
end
