# frozen_string_literal: true

class LandingPageController < ApplicationController
  def home; end
end
