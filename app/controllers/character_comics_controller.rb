# frozen_string_literal: true

class CharacterComicsController < ApplicationController
  before_action :permit_params

  def get
    ensure_input or return

    comics_data = get_comics_featuring_character(
      @permit_params[:name],
      page: @permit_params[:page]
    )

    ensure_data(comics_data) or return

    parse_comics_response(comics_data)
    append_comics_upvotes

    render :show
  end

  private

  def permit_params
    @permit_params ||= params.permit(:name, :page)
  end

  def ensure_input
    if !@permit_params[:name] || @permit_params[:name]&.empty?
      redirect_to :root, notice: 'Type in the name of your favourite character in the field above!'
      false
    else
      true
    end
  end

  def ensure_data(data)
    if data
      true
    else
      redirect_to :root, notice: 'No comics found!'
      false
    end
  end

  def get_comics_featuring_character(name, page: nil)
    marvel = QueryMarvel.new
    characters_ids = marvel.fetch_characters(name)[:results].collect { |char| char['id'] }
    return if characters_ids.empty?

    options = {}
    page && options[:page] = page

    marvel.fetch_comics_by_character(characters_ids, options)
  end

  def parse_comics_response(comics_data)
    @comics = comics_data[:results].collect do |comic|
      {
        id: comic['id'],
        title: comic['title'],
        cover: comic.dig('thumbnail', 'path')
      }
    end

    @next_page = comics_data[:next_page] if comics_data[:next_page] < comics_data[:total_pages]
    @previous_page = comics_data[:previous_page] if (comics_data[:previous_page]).positive?
  end

  def append_comics_upvotes
    comics_ids = @comics.collect { |comic| comic[:id] }.sort
    favourites = Favourite.where(marvel_comic_id: comics_ids)

    @comics = @comics.map do |comic|
      respective_favourite = find_in_favourites(comic, favourites)

      comic[:upvotes] = respective_favourite&.upvotes || 0
      comic
    end
  end

  def find_in_favourites(comic, favourites)
    favourites.find { |favourite| favourite.marvel_comic_id == comic[:id] }
  end
end
