#!/bin/bash

set -eu

yarn install --check-files
bundle install
rails db:create
rails db:migrate

echo "Setup complete"
