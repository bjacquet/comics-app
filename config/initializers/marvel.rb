# frozen_string_literal: true

require 'marvel'

Marvel.configure do |config|
  config.api_endpoint = ENV.fetch('MARVEL_API_ENDPOINT')
  config.private_key = ENV.fetch('MARVEL_PRIVATE_KEY')
  config.public_key = ENV.fetch('MARVEL_PUBLIC_KEY')
end
