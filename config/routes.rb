# frozen_string_literal: true

Rails.application.routes.draw do
  root 'landing_page#home'

  get '/character_comics', to: 'character_comics#get'
  post '/favourites/upvote', to: 'favourites#upvote'
end
