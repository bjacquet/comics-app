# frozen_string_literal: true

class CreateFavourites < ActiveRecord::Migration[6.0]
  def change
    create_table :favourites do |t|
      t.bigint :marvel_comic_id
      t.bigint :upvotes, default: 0

      t.timestamps
    end

    add_index :favourites, :marvel_comic_id, unique: true
  end
end
