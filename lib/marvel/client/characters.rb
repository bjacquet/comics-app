# frozen_string_literal: true

module Marvel
  class Client
    module Characters
      def get_characters_by_name(name)
        get('characters', nameStartsWith: name)
      end
    end
  end
end
