# frozen_string_literal: true

module Marvel
  class Client
    module Comics
      def get_comics_by_characters(characters_ids, options = {})
        characters_ids = Array(characters_ids).first(10).join(',')
        get(
          'comics',
          options.merge({
                          characters: characters_ids,
                          orderBy: 'onsaleDate'
                        })
        )
      end
    end
  end
end
