# frozen_string_literal: true

require 'marvel/client/characters'
require 'marvel/client/comics'

module Marvel
  class Client
    include Marvel::Client::Characters
    include Marvel::Client::Comics

    attr_reader :api_endpoint, :api_key, :hash, :timestamp

    def initialize
      @api_endpoint = Marvel.config.api_endpoint
      @api_key = Marvel.config.public_key
      @hash = marvel_hash
      @timestamp = time_stamp
    end

    def get(path, options = {})
      request(:get, path, options)
    end

    # private

    def connection
      @connection ||=
        Faraday.new(@api_endpoint) do |conn|
          conn.ssl.verify = true
          conn.adapter Faraday.default_adapter
        end
    end

    def request(method, path, options)
      options[:ts] = timestamp
      options[:apikey] = api_key
      options[:hash] = hash

      response = connection.send(method) do |request|
        request.url(path, options)
      end

      JSON.parse(response.body) if response.body.present?
    end

    def time_stamp
      @timestamp ||= Time.now.to_i.to_s
    end

    def marvel_hash
      Digest::MD5.hexdigest(
        time_stamp +
          Marvel.config.private_key +
          Marvel.config.public_key
      ).to_s
    end
  end
end
