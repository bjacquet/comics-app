# frozen_string_literal: true

require 'marvel/client'

module Marvel
  include ActiveSupport::Configurable

  class << self
    def configure
      yield config
    end
  end
end
