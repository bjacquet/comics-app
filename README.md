# Comic App

The **Comic App** is a place were comic readers, and everyone else, can search for comics of a given character. Users can vote their favourite issues. Most popular issues are shown in a red frame for easy finding.

## System configuration

**Note!** It's assumed that package management tools (like. **bundle**, **yarn**, etc) and **Ruby** to be already intalled in the system.

System setup is performed by running the `build.sh` script

```shell
./build.sh
```

It will download all necessary Ruby gems; install them; and create a database. The message _Setup complete_ is shown at the end.

To launch the project launch the Rails server

```shell
rails s
```

and then open it on http://localhost:3000/.

## Incompleted tasks

### User & Session management

When I first read the requirements I though _there will be Users and each one will have it's favourite comics_. But the next page read _"User management and authentication is not required"_ and so I scrapped that idea. I assumed that anyone could upvote and that red frames where for any comic that got voted.

My **User** model ideia was like so

![User Model](docs/user_model.png)

Once I decided to not implement this model I left the _"30 minute cookie session"_ for later which then I totaly forgot to do it.

### API caching

The comics domain is very static. New characters or issues aren't updated every hour nor are they released on a daily basis. Thus it makes sense to minimize the amount of requests we make to Marvel's API. Most likely if we search for _Wolverine_'s comics today we'll get the exact same results tomorrow. Thus it is safe to cache responses for up to 24 hours. Not only that but our _PREVIOUS_ button is very likely be clicked, thus repeating the same request from a few minutes (or seconds!) ago.

### Production capable code

Speaking of the API, the code is always assuming that every request follows the happy path. However things are not always like that. The code handling the requests to Marvel's API should be checking the response status and act accordingly. It should be also ready to rescue any exception that **Faraday** might raise.

### Frontend niceties

As I said on the interview my I'm not used to create things flying about the page. The way I went about to show the comic title and the heart image is not something I am proud of.

### Highlight most popular comics

Any **Comic** with a positive number of upvotes gets highlighted by a red frame. I think a better feature would be to highligh only the top 50% of comics with most upvotes. We could even have three different shades of red to highlight the top 50%, top 20%, and top 5%.

### Tryout Svelte

This is a personal one, I wanted to use this oportunity to tryout **Svelte**. But I had so much time wasted trying to install other libraries that I was afraid of not having enough time to learn how to use this framework.

## Problems during development

### Third-party libraries dependency

I ran into trouble with yarn* after trying to add the **TailwindCSS** package — which I couldn't! When trying to launch the server again it would complain abount packages mismatch and required to run `yarn install --check-files` again. I hope you don't get thes issue as well.

I ended up having to add the library dependency directly in the view — which one should never do.

I have a personal dislike of using frameworks like Bootstrap as they make every website look the same.

### Marvel's API timestamp

I don't know why Marvel's API request for a timestamp parameter. I could not understand how this adds any privacy/security to protect the request. I can only imagine that this allows them to not having to store any user information on their side.

When I first queried their API with `curl` I was constantly getting back errors. I took me a while to figure out I was using the `md5` command the wrong way.

### Feature testing

This was also the first time I wrote feature tests. The two top referenced gems for this were **Cucumber** and **Capybara**. I choose the latter because the syntax closely resembled **RSpec**.

There are two tests failing. They try to simulate a user upvoting a comic. This functionality relies on a JavaScript function and that my be the reason why "plain" Capybara code couldn't invoque the `onClick` event. I understand know that that is dependent on configuring a JavaScript driver.

## Lessons learn

### Time management

This exercise looks simple enough for one week's work. Although I didn't have a week's time to work on I thought I could still fulfill all requirements till the deadline.

One thing I would do differently now is to plan dedicated time slots to work on this instead of "whenever I'm free". That way not only will I have that time available as I (probably) won't get interrupted much.

### TailwindCSS

I wanted to have used TailwindCSS on previous project, when we revamped the frontend. However the client hired a fulltime designer which created a full Design System. So now was my opportunity to give TailwindCSS a ggo and I loved it. I do not dislike CSS but since we already use partials to decompose and struture the webpage, it makes perfect sense define the style directly in partial file.

### Capybara

I didn't wrote a lot of feature tests but I got a taste of what Capybara is able to do. I will definetly push for its use on my current project.
