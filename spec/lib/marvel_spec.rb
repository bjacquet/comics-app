# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Marvel::Client do
  subject { Marvel::Client.new }

  describe '#initialize' do
    it 'with default values' do
      expect(subject.api_endpoint).to eq(ENV.fetch('MARVEL_API_ENDPOINT'))
      expect(subject.api_key).to eq(ENV.fetch('MARVEL_PUBLIC_KEY'))
      expect(subject.hash).to be_present
      expect(subject.timestamp).to be_present
    end
  end
end
