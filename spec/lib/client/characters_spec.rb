# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Marvel::Client::Characters do
  subject { Marvel::Client.new }

  describe '#get_characters_by_name' do
    it 'calls Marvel::Client#get' do
      path = 'characters'
      options = { nameStartsWith: 'a name' }

      allow(subject).to receive(:get)
      subject.get_characters_by_name('a name')

      expect(subject).to have_received(:get).with(path, options)
    end
  end
end
