# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QueryMarvel do
  subject { QueryMarvel.new }
  let(:expected_response_keys) { %i[current_page next_page previous_page results] }

  describe '#fetch_characters' do
    let(:get_characters_by_name_url) { %r{gateway\.marvel\.com/v1/public/characters} }
    let(:get_characters_by_name_response) do
      filename = Rails.root.join('spec/stubs/response_get_characters_by_name.txt')
      File.new(filename)
    end
    let(:character_name) { 'legion' }

    it 'returns characters matching name' do
      stub_request(:get, get_characters_by_name_url)
        .to_return(get_characters_by_name_response)

      result = subject.fetch_characters(character_name)

      expect(result.keys).to include(*expected_response_keys)
      expect(result[:results].first['name'].downcase).to eq(character_name)
    end
  end

  describe '#fetch_comics_by_character' do
    let(:get_comics_by_character_url) { %r{gateway\.marvel\.com/v1/public/comics} }
    let(:get_comics_by_character_response) do
      filename = Rails.root.join('spec/stubs/response_get_comics_by_character.txt')
      File.new(filename)
    end
    let(:get_comics_by_character_page_2_response) do
      filename = Rails.root.join('spec/stubs/response_get_comics_by_character_page_2.txt')
      File.new(filename)
    end
    let(:character_id) { 1_009_399 }

    it 'returns comics of character' do
      stub_request(:get, get_comics_by_character_url)
        .to_return(get_comics_by_character_response)

      result = subject.fetch_comics_by_character(character_id)

      expect(result).to_not be_empty
      expect(result.keys).to include(*expected_response_keys)
      expect(result[:results].empty?).to be false
    end

    it 'returns page two of comics of character' do
      stub_request(:get, get_comics_by_character_url)
        .with(query: hash_including({ 'offset' => '20' }))
        .to_return(get_comics_by_character_page_2_response)

      result = subject.fetch_comics_by_character(character_id, page: 2)

      expect(result.keys).to include(*expected_response_keys)
      expect(result[:results].empty?).to be false
      expect(result[:current_page]).to eq(2)
    end
  end
end
