# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Searches for comics', type: :feature do
  let(:get_characters_by_name_url) { %r{gateway\.marvel\.com/v1/public/characters} }
  let(:get_comics_by_character_url) { %r{gateway\.marvel\.com/v1/public/comics} }
  let(:get_characters_by_name_response) do
    filename = Rails.root.join('spec/stubs/response_get_characters_by_name.txt')
    File.new(filename)
  end
  let(:get_comics_by_character_response) do
    filename = Rails.root.join('spec/stubs/response_get_comics_by_character.txt')
    File.new(filename)
  end
  let(:get_characters_by_name_not_found_response) do
    filename = Rails.root.join('spec/stubs/response_get_characters_by_name_not_found.txt')
    File.new(filename)
  end

  scenario 'with real hero' do
    stub_request(:get, get_characters_by_name_url)
      .to_return(get_characters_by_name_response)
    stub_request(:get, get_comics_by_character_url)
      .to_return(get_comics_by_character_response)

    visit root_path
    fill_in 'name', with: 'Legion'
    form = find(id: 'search')
    page.submit form

    expect(page.all('img').size).to eq(21)

    next_button = find(id: 'next-page-button')
    expect(next_button).to be_truthy
  end

  scenario 'with no input' do
    visit root_path
    form = find(id: 'search')
    page.submit form

    expect(page).to have_content('Type in the name of your favourite character in the field above!')
  end

  scenario 'with an yet to be hero' do
    stub_request(:get, get_characters_by_name_url)
      .to_return(get_characters_by_name_not_found_response)

    visit root_path
    fill_in 'name', with: 'Street Bees'
    form = find(id: 'search')
    page.submit form

    expect(page).to have_content('No comics found!')
  end
end
