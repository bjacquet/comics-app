# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Upvotes a comic', type: :feature do
  let(:get_characters_by_name_url) { %r{gateway\.marvel\.com/v1/public/characters} }
  let(:get_comics_by_character_url) { %r{gateway\.marvel\.com/v1/public/comics} }
  let(:get_characters_by_name_response) do
    filename = Rails.root.join('spec/stubs/response_get_characters_by_name.txt')
    File.new(filename)
  end
  let(:get_comics_by_character_response) do
    filename = Rails.root.join('spec/stubs/response_get_comics_by_character.txt')
    File.new(filename)
  end

  scenario 'creates a Favourite' do
    stub_request(:get, get_characters_by_name_url)
      .to_return(get_characters_by_name_response)
    stub_request(:get, get_comics_by_character_url)
      .to_return(get_comics_by_character_response)

    visit character_comics_path(name: 'legion')
    find_button(id: '71466').click

    expect(Favourite.count).to eq(1)
    expect(Favourtie.first.upvotes).to eq(1)
  end

  scenario 'reduces Favourite upvotes to zero' do
    stub_request(:get, get_characters_by_name_url)
      .to_return(get_characters_by_name_response)
    stub_request(:get, get_comics_by_character_url)
      .to_return(get_comics_by_character_response)

    visit character_comics_path(name: 'legion')
    find_button(id: '71466').click
    find_button(id: '71466').click

    expect(Favourite.count).to eq(1)
    expect(Favourtie.first.upvotes).to eq(0)
  end
end
